﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ImageSorter
{
    class FileHelper
    {

        private string rootPath;
        private string resolutionsFilePath;
        private string sortedImagesPath;
        private string unsortedImagesPath;

        private Logger logger;

        public FileHelper(Logger logger)
        {
            this.logger = logger;

            rootPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            resolutionsFilePath = rootPath + @"\resolutions.txt";
            sortedImagesPath = rootPath + @"\Sorted Images";
            unsortedImagesPath = rootPath + @"\Images";

            if (!File.Exists(resolutionsFilePath))
            {
                File.Create(resolutionsFilePath);
            }

            if (!Directory.Exists(sortedImagesPath))
            {
                Directory.CreateDirectory(sortedImagesPath);
            }

            if (!Directory.Exists(unsortedImagesPath))
            {
                Directory.CreateDirectory(unsortedImagesPath);
            }
        }

        public void CopyImage(ProcessedImage image)
        {
            try
            {
                string source = unsortedImagesPath + @"\" + image.SourceFileName;
                string destination = image.DestinationFilePath + @"\" + image.SourceFileName;

                if (!File.Exists(destination))
                {
                    File.Copy(source, destination);
                    logger.Log(image.SourceFileName + " successfully processed.", true);
                }
                else
                {
                    logger.Log(image.SourceFileName + " already exists. Skipping.", true);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<string> ReadFileNames()
        {
            List<string> fileNames = new List<string>();

            DirectoryInfo directoryInfo = new DirectoryInfo(unsortedImagesPath);

            FileInfo[] files = directoryInfo.GetFiles("*.*");

            foreach(FileInfo file in files)
            {
                fileNames.Add(file.Name);
            }

            return fileNames;
        }

        public Image ReadImage(string fileName)
        {
            try
            {
                Image img = Image.FromFile(unsortedImagesPath + @"\" + fileName);
                return img;
            }
            catch (Exception)
            {
                Console.WriteLine(fileName + " doesn't appear to be an image.");
                logger.Log(fileName + " doesn't appear to be an image.", false);
                return null;
            }
        }

        public string CreateResolutionDirectory(Resolution resolution)
        {
            string resolutionPath;

            if (resolution.Favorite)
            {
                resolutionPath = sortedImagesPath + @"\Favorite Resolutions\" + resolution.ToString();
            }
            else
            {
                resolutionPath = sortedImagesPath + @"\Other Resolutions\" + resolution.ToString();
            }

            if (!Directory.Exists(resolutionPath))
            {
                Directory.CreateDirectory(resolutionPath);
            }

            return resolutionPath;
        }

        public string[] ReadResolutions()
        {
            try
            {
                return File.ReadAllLines(resolutionsFilePath);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
