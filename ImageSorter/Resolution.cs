﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageSorter
{
    class Resolution
    {
        public int X { get; set; }
        public int Y { get; set; }

        public bool Favorite { get; set; }
        
        public override string ToString()
        {
            return X + "x" + Y;
        }

        public override bool Equals(object obj)
        {
            return ToString().Equals(((Resolution) obj).ToString());
        }

        public override int GetHashCode()
        {
            int hash = 0;

            Int32.TryParse(ToString(), out hash);

            return hash;
        }
    }
}
