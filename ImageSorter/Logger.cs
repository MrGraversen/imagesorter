﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ImageSorter
{
    class Logger
    {
        private string logPath;

        private StreamWriter streamWriter;

        private bool enableLog;
        private bool enableConsole;

        public Logger(bool log, bool console)
        {
            enableLog = log;
            enableConsole = console;

            logPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\log.txt";

            if (enableLog)
            {
                if (!File.Exists(logPath))
                {
                    FileStream file = File.Create(logPath);
                    file.Close();
                }
                else
                {
                    File.Delete(logPath);

                    FileStream file = File.Create(logPath);
                    file.Close();
                }

                streamWriter = File.AppendText(logPath);
            }
            else
            {
                File.Delete(logPath);
            }

        }

        public void Log(string message, bool console)
        {
            if (enableLog)
            {
                streamWriter.WriteLine("{0} - {1}: {2}", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString(), message);
            }

            if (console && enableConsole)
            {
                Console.WriteLine(message);
            }
        }

        public void InsertNewLine()
        {
            if (enableLog)
            {
                streamWriter.WriteLine("");
            }
        }

    }
}
