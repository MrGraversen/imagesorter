﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageSorter
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Image Sorter";
            Console.WriteLine("Hello and welcome. Let's get to it.\n");

            int timeStamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;

            ImageSorter.Instance.SortImages();

            int completionTime = (Int32) (DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds - timeStamp;

            Console.WriteLine("\n--- DONE in " + (completionTime) + " seconds ---");
            Console.ReadKey();
        }
    }
}
