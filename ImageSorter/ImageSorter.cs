﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ImageSorter
{
    internal class ImageSorter
    {

        private static ImageSorter _instance;
        private static readonly object ThreadLock = new object();

        private Logger logger;
        private FileHelper fileHelper;

        private Dictionary<Resolution, string> resolutionsDictionary;

        private ImageSorter()
        {
            resolutionsDictionary = new Dictionary<Resolution, string>();
            
            Console.WriteLine("\nPress Y if you want logging enabled. Press something else if you don't.");
            bool log = Console.ReadKey().Key == ConsoleKey.Y;

            Console.WriteLine("\nPress Y if you want console output enabled. Press something else if you don't.");
            bool console = Console.ReadKey().Key == ConsoleKey.Y;

            logger = new Logger(log, console);
            fileHelper = new FileHelper(logger);
        }

        public static ImageSorter Instance
        {
            get
            {
                lock (ThreadLock)
                {
                    return _instance ?? (_instance = new ImageSorter());
                }
            }
        }

        public void SortImages()
        {
            ReadResolutions();

            List<string> fileNameList = fileHelper.ReadFileNames();

            Console.WriteLine("\n\nFound " + fileNameList.Count + " files. Time to get to work.\n");
            logger.Log("Found " + fileNameList.Count + " files.", false);
            logger.InsertNewLine();

            foreach (string fileName in fileNameList)
            {
                Sort(fileName);
            }
        }

        private void Sort(string fileName)
        {
            Image image = fileHelper.ReadImage(fileName);

            if (image != null)
            {
                Resolution resolution = new Resolution
                {
                    X = image.Width,
                    Y = image.Height,
                    Favorite = false
                };

                if (!resolutionsDictionary.ContainsKey(resolution))
                {
                    resolutionsDictionary.Add(resolution, fileHelper.CreateResolutionDirectory(resolution));
                }
                
                ProcessedImage processedImage = new ProcessedImage
                {
                    Image = image,
                    SourceFileName = fileName,
                    DestinationFilePath = resolutionsDictionary[resolution]
                };

                fileHelper.CopyImage(processedImage);
            }

            if (image != null) image.Dispose();
        }

        private void ReadResolutions()
        {
            foreach (var resolutionString in fileHelper.ReadResolutions())
            {
                string re1 = "(\\d+)";	// Integer Number 1
                string re2 = "(x)";	    // x
                string re3 = "(\\d+)";	// Integer Number 2

                if (new Regex(re1 + re2 + re3).IsMatch(resolutionString))
                {
                    Console.WriteLine("Adding " + resolutionString + " as favorite resolution.");
                    logger.Log("Adding " + resolutionString + " as favorite resolution.", false);

                    string[] components = resolutionString.Split(new[] { 'x' });

                    int x = 0;
                    int y = 0;

                    Int32.TryParse(components[0], out x);
                    Int32.TryParse(components[1], out y);

                    Resolution resolution = new Resolution
                    {
                        X = x,
                        Y = y,
                        Favorite = true
                    };

                    resolutionsDictionary.Add(resolution, fileHelper.CreateResolutionDirectory(resolution));
                }
                else
                {
                    logger.Log(resolutionString + " isn't a valid resolution.", true);
                }
            }
        }
    }
}
