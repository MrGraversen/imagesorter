﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace ImageSorter
{
    class ProcessedImage
    {
        public string SourceFileName { get; set; }
        public string DestinationFilePath { get; set; }

        public Image Image { get; set; }

        public Resolution Resolution
        {
            get
            {
                return new Resolution
                {
                    X = Image.Width,
                    Y = Image.Height
                };
            }
        }
    }
}
